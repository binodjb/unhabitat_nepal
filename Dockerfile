FROM rocker/r-ver:4.1.0
 
# Install dependency libraries

RUN apt-get update && \ 
    apt-get install -y software-properties-common && \
    apt-get update && \
    add-apt-repository ppa:ubuntugis/ubuntugis-unstable && \
    apt-get install -y \
    libxml2-dev \
    libudunits2-dev \
    libssh2-1-dev \
    libcurl4-openssl-dev \
    libssl-dev \
    libsasl2-dev \
	libv8-dev \
    libudunits2-dev \ 
    libgdal-dev \
    libgeos-dev \ 
    libproj-dev && \ 
    R -e "options(Ncpus = 4, repo='https://packagemanager.rstudio.com/all/__linux__/focal/latest'); install.packages(c('bs4Dash', 'ragg',  'htmltools', 'firebase','dplyr', 'httr', 'leaflet', 'rmarkdown', 'knitr', 'shiny', 'jsonlite', 'sf', 'devtools', 'readr', 'plotly', 'RColorBrewer', 'lattice', 'shinyjs', 'shinyBS', 'tidyverse', 'ggthemes', 'leaflet.extras', 'shinydashboard', 'janitor', 'readxl', 'devtools', 'DT', 'shinyWidgets', 'shinyFiles', 'writexl', 'Cairo', 'pbkrtest', 'conquer', 'pacman'), dependencies = TRUE);devtools::install_github(c('bbc/bbplot'), dependencies = TRUE); library(dplyr); library(RColorBrewer);library(sf);library(jsonlite); library(httr); library(leaflet)" && \
    mkdir -p /home/unhabitat_app && chmod -R 755 /home/unhabitat_app && chmod -R 777 /tmp

    
COPY . /home/unhabitat_app

# expose port on Docker container
EXPOSE 4040

# run app.R as localhost and on exposed port in Docker container

CMD ["R", "-e", "shiny::runApp('/home/unhabitat_app/', host='0.0.0.0', launch.browser = F, port=4040)"]