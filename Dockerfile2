FROM rocker/r-ver:4.0.2
 
# Install dependency libraries

RUN apt-get update && apt-get install -y  \
            libxml2-dev \
            libudunits2-dev \
            libssh2-1-dev \
            libcurl4-openssl-dev \
            libssl-dev \
            libsasl2-dev \
	          libv8-dev \
            libudunits2-dev \ 
            libgdal-dev \
            libgeos-dev \ 
            libproj-dev 

Run apt-get install -y \ 
            libcairo2-dev \
            libxt-dev 
            
# install needed R packages
RUN R -e "options(Ncpus = 4); install.packages(c('dplyr', 'httr', 'leaflet', 'rmarkdown', 'flexdashboard', 'knitr', 'shiny', 'jsonlite', 'sf', 'devtools', 'readr', 'plotly', 'RColorBrewer', 'lattice', 'shinyjs', 'shinyBS', 'tidyverse', 'ggthemes', 'leaflet.extras'), repo='https://cloud.r-project.org', dependencies = TRUE)"

RUN R -e "options(Ncpus = 4); install.packages(c('shinydashboard', 'janitor', 'readxl', 'devtools'), repo='https://cloud.r-project.org', dependencies = TRUE)"


RUN R -e "options(Ncpus = 4); install.packages(c('DT', 'shinyWidgets', 'shinyFiles', 'writexl'), repo='https://cloud.r-project.org', dependencies = TRUE)"

RUN R -e "options(Ncpus = 4); install.packages(c('Cairo', 'pbkrtest', 'conquer'), repo='https://cloud.r-project.org')"

RUN R -e "options(Ncpus = 4); devtools::install_github(c('bbc/bbplot'), dependencies = TRUE)"

# make directory and copy flexdashboard file in it
RUN mkdir -p /home/unhabitat_app
COPY .  /home/unhabitat_app

# make all app files readable (solves issue when dev in Windows, but building in Ubuntu)
RUN chmod -R 755 /home/unhabitat_app

RUN chmod -R 777 /tmp

# expose port on Docker container
EXPOSE 4040

RUN R -e "library(dplyr); library(RColorBrewer);library(sf);library(jsonlite); library(httr); library(leaflet)"

# run app.R as localhost and on exposed port in Docker container

CMD ["R", "-e", "shiny::runApp('/home/unhabitat_app/', host='0.0.0.0', launch.browser = F, port=4040)"]